# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JobItem(scrapy.Item):
	companies = scrapy.Field()
	job_title = scrapy.Field()
	url = scrapy.Field()
	is_intern = scrapy.Field()