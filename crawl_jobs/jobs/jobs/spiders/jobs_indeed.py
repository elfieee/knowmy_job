# -*- coding: utf-8 -*-
'''
last modified on Jan 05, 2016
Yun
'''

import scrapy
import csv
import codecs
from re import split
from jobs.items import JobItem
#from __future__ import print_function
#import string


def generate_start_urls():
    keywords_ex = [u'senior', u'manager', u'coordinator', u'co\xf6rdinator']
    keywords_in = []#u'python']
    companies = []
    with codecs.open('/Users/yun/Documents/programming/KnowMy_job/companylist.csv',\
              'rb', encoding='utf-8') as f:
        inputs = csv.reader(f, delimiter=',')
        for row in inputs:
            temp = list(row)
            try:
                name = temp[0].rstrip('\n')
            except:
                pass
            if not name.isspace():
                companies.append(name)

    urls = [''.join(['https://www.indeed.nl/vacatures?as_and=',
        '+'.join(keywords_in), '&as_phr=&as_any=&as_not=',
        '+'.join(keywords_ex), '&as_ttl=&as_cmp=', company,
        '&jt=all&st=&radius=50&l=Amsterdam&fromage=any&limit=10&sort=&psf=advsrch'])\
                for company in companies]
    #print(urls)
    return urls



if 'name' == '__main__':
    generate_start_urls()



class indeedSpider(scrapy.Spider):
    name = 'indeed'

    def start_requests(self):
        urls = generate_start_urls()

        ### some test urls ###
        #urls = ['https://www.indeed.nl/vacatures?as_and=&as_phr=&as_any=&as_not=senior+manager+coordinator&as_ttl=&as_cmp=t-mobile&jt=all&st=&radius=50&l=Amsterdam&fromage=any&limit=10&sort=&psf=advsrch',]
        #urls = ['https://www.indeed.nl/vacatures?q=telegraaf+sales&l=Amsterdam']

        for url in urls:
            # yield jobs in first page
            yield scrapy.Request(url, callback=self.parse_jobs)
            # check if there are multiple pages
            yield scrapy.Request(url, callback=self.parse_pages)


    def parse_pages(self, response):
        # explore everything if there are multiple pages
        next_pages = [response.urljoin(href) for href in \
                response.xpath("//td/div[@class='pagination']/a/@href").\
                extract()[:-1]]
        for url in next_pages:
            yield scrapy.Request(url, callback=self.parse_jobs)


    def parse_jobs(self, response):
        # gets all jobs in page

        # replaces <b> and </b> so scrapy doesn't ignore words in between
        response = response.replace(body=response.body.replace(b'<b>', b''))
        response = response.replace(body=response.body.replace(b'<b/>', b''))

        # get all job positions in page
        urls = [response.urljoin(u)  for u in \
                response.xpath("//tr/td/div[contains(@class,'row') \
                and contains(@class,'result')]/h2[@class='jobtitle']\
                /a/@href").extract()]
        job_titles = response.xpath("//tr/td/div[contains(@class,'row') and\
                contains(@class,'result')]/h2[@class='jobtitle']/a/\
                text()").extract()
        companies = response.xpath("//tr/td/div[contains(@class,'row') and \
                contains(@class,'result')]/span[@class='company']").\
                re('(>\n)(.+)(?:</)')[1::2]

        # compare if the lengths of these arrays are equal
        # if not, raise an error
        print(len(urls))
        print(len(job_titles))
        print(len(companies))
        assert (len(urls)==len(job_titles) and len(urls)==len(companies)),\
                'Equal lengths in all arrays!'

        for ct in range(len(urls)):
            item = JobItem()
            item['companies'] = companies[ct]
            item['job_title'] = job_titles[ct]
            item['url'] = urls[ct]
            internships = {'stagiair', 'stage', 'intern', 'internsip',
                    'trainee', 'traineeship'}
            if list(internships.intersection(split(', |. |/| | / ', job_titles[ct]))):
                item['is_intern'] = 'yes'
            else:
                item['is_intern'] = 'no'
            yield item
