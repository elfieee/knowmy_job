'''This is to clean up the text file extrated from the original pdf
by Yun Li
last modified on Dec 26, 2016'''

import csv

companies = []
with open('companylist.txt', 'rb') as f_in:
    lines = f_in.readlines()[5:]

    #name_ct = 0 # how many company names are already enumerated
    nr_ct = 0 # how many corresponding numbers are enumerated
    for company in lines:
        if not (company == '\n' or 'Pagina' in company or \
                'Inschrijfnummer' in company or 'Registration number' in company \
                or 'Chamber of Commerce' in company or company[:-1].isdigit()):
            companies.append([company])
            #name_ct += 1
        if len(company) > 4 and company[:-1].isdigit():
            companies[nr_ct].append(int(company))
            nr_ct += 1

with open('companylist.csv', 'wb') as f_out:
    output = csv.writer(f_out, delimiter=',')
    for row in companies:
        output.writerow(row)
